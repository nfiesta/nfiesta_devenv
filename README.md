# gitlab CI/CD environment

## get docker image

Create docker image

```bash
docker build -t registry.gitlab.com/nfiesta/nfiesta_devenv:pg12 .
sudo docker build --no-cache --progress plain -t registry.gitlab.com/nfiesta/nfiesta_devenv:pg12 -f Dockerfile_pg12 .
sudo docker build --no-cache --progress plain -t registry.gitlab.com/nfiesta/nfiesta_devenv:pg16 -f Dockerfile_pg16 .
```

push
```bash
docker login registry.gitlab.com
docker push registry.gitlab.com/nfiesta/nfiesta_devenv:pg12
```

or pull directly from repository

```bash
docker pull registry.gitlab.com/nfiesta/nfiesta_devenv:pg12
```

## run image locally

```bash
docker image ls
docker run -i -t -v ${pwd}:/home/vagrant/work_dir -p 5432:5432 --shm-size=1g --name nfiesta_dev --hostname nfiesta_dev registry.gitlab.com/nfiesta/nfiesta_devenv:pg12 /bin/bash
docker container ls -a
docker container rm -v nfiesta_dev
```

## set-up local environment and run CI/CD jobs locally 

```
sudo dpkg-reconfigure tzdata
git clone ...
cd ...
bash provision.sh
```

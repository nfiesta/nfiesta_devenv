Thanks to Rolf Meile

# Setup of Postgres at an earlier stage

```bash
# root@vapricot
dnf install -y https://download.postgresql.org/pub/repos/yum/reporpms/EL-8-x86_64/pgdg-redhat-repo-latest.noarch.rpm
dnf install -y https://dl.fedoraproject.org/pub/epel/epel-release-latest-8.noarch.rpm

# Disable built-in CentOS Postgresql module
dnf list "postgresql*-server"
dnf -qy module disable postgresql

# Do not install the client packages dnf install postgresql12

# Install the server packages
dnf install  postgresql12-server


# Initialize the database and enable automatic start:
/usr/pgsql-12/bin/postgresql-12-setup initdb
systemctl enable postgresql-12
systemctl start postgresql-12
systemctl status postgresql-12

su postgres
psql -c "SELECT version();"

# poweroff
poweroff
```


# Nfiesta Adaptations & Setup


Add your user to wheel group and adjust
```bash
# wheel group for sudo usage on CentOS
usermod -aG wheel vagrant
```bash

Add your user to sudoers, wheel group and allow to act as root and postgres user without asking password

```bash
# edit env script of vagrant:
# 1) add postgres paths
# 2) to do su with sudo to be somebody else
nano /home/vagrant/.bashrc

        # User specific environment
       PATH="$HOME/.local/bin:$HOME/bin:$PATH"
       PATH="$PATH:/usr/pgsql-12/bin"
       PATH="$PATH:/usr/pgsql-12/include"
       export PATH

       alias su='sudo su -'
```

Install: corresponding postgresql-server-dev(?) plpython3 and python3-numpy

```bash
#root@vfiesta
# do we have the stuff needed? (no need for postgresql-12-postgis-3-scripts since Postgis is already installed from installer)

# CentOS specific (ubuntu would be postgresql-server-dev)
dnf list postgresql12-devel
dnf install -y postgresql12-devel

dnf list *plpython3*
dnf install -y postgresql12-plpython3

dnf list python3-numpy*                
dnf install -y python3-numpy
               
dnf list python3-psycopg*                                                  
dnf install -y python3-psycopg2
```

Additional steps on CentOS installation conerning repos

```bash
#root@vfiesta
# repository stuff to correctly enable make with centos8/pg12
dnf install -y redhat-rpm-config
# compiler stuff to correctly enable make with centos8/pg12
dnf install -y ccache
dnf install -y llvm
dnf install -y clang
```


Build, install and test extension [nfiesta](https://gitlab.com/nfiesta/nfiesta_htc)

```bash
# vagrant@vfiesta
cd $HOME
git clone https://gitlab.com/nfiesta/nfiesta_htc.git
cd nfiesta_htc
make
sudo PATH="/usr/pgsql-12/bin:${PATH}" make install

# run tests
make installcheck
# all tests ok
```bash

Build, install and test extension [nfiesta](https://gitlab.com/nfiesta/nfiesta_pg)

```bash
# vagrant@vfiesta
cd $HOME
git clone https://gitlab.com/nfiesta/nfiesta_pg.git
cd nfiesta_pg
make
sudo PATH="/usr/pgsql-12/bin:${PATH}" make install

# run tests
make installcheck

# all tests ok except test fn_inverse (minor bug; no effect on estimates; CentOS numpy implementation?)
nano /home/vagrant/nfiesta_pg/regression.diffs
nano /home/vagrant/nfiesta_pg/regression.out

sudo poweroff
```

#! /usr/bin/env python
# -*- coding: utf-8 -*-

import subprocess
import urllib.parse
import sys
import os
import re

subprocess.check_call([sys.executable, "-m", "pip", "install", "requests"])
import requests

def get_wiki(wiki_git_url):
    print('getting wiki %s' % wiki_git_url)
    wiki_dir = re.findall(r'([^/]+.wiki).git', wiki_git_url)[0]
    print('checking directory %s exists' % wiki_dir)
    if os.path.exists(wiki_dir):
        os.chdir(wiki_dir)
        subprocess.check_call(['git', 'pull'])
        os.chdir('../')
    else:    
        subprocess.check_call(['git', 'clone', wiki_git_url])

get_wiki('https://gitlab.com/nfiesta.wiki.git')

api_url = 'https://gitlab.com/api/v4/groups/nfiesta/projects'
response = requests.get(api_url)
projects = response.json()

for project in projects:
    wiki_url = 'https://gitlab.com/api/v4/projects/%s/wikis' % urllib.parse.quote_plus('%s/%s' % ('nfiesta', project['name']))
    print('checking project %s' % (project['name']))
    wiki = requests.get(wiki_url).json()
    if (len(wiki) > 0):
        wiki_git = 'https://gitlab.com/nfiesta/%s.wiki.git' % project['name']
        get_wiki(wiki_git)

print('finished')
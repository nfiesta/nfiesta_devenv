Script downloading all wiki pages for all projects to the current directory:

[local_nFIESTA_wiki.py](https://gitlab.com/nfiesta/nfiesta_devenv/-/blob/main/local_nFIESTA_wiki/local_nFIESTA_wiki.py?ref_type=heads)

# installation and usage

## Windows (powershell)

```powershell
winget install python
```

```powershell
mkdir local_nFIESTA_wiki
cd local_nFIESTA_wiki
curl https://gitlab.com/api/v4/projects/nfiesta%2fnfiesta_devenv/repository/files/local_nFIESTA_wiki%2Flocal_nFIESTA_wiki.py/raw?ref=main -o local_nFIESTA_wiki.py
python3 local_nFIESTA_wiki.py # clone repositories when running first time
python3 local_nFIESTA_wiki.py # pull updates when repeated
```

## Linux (bash)

```bash
sudo apt install python3 python3-pip
```

```bash
mkdir local_nFIESTA_wiki
cd local_nFIESTA_wiki
curl https://gitlab.com/api/v4/projects/nfiesta%2fnfiesta_devenv/repository/files/local_nFIESTA_wiki%2Flocal_nFIESTA_wiki.py/raw?ref=main -o local_nFIESTA_wiki.py
python3 local_nFIESTA_wiki.py # clone repositories when running first time
python3 local_nFIESTA_wiki.py # pull updates when repeated
```
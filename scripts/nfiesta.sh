# Edit the following to change the name of the database user that will be created:
APP_DB_USER=adm_nfiesta
APP_DB_PASS=local_nfiesta_passwd
APP_DB_NAME=nfiesta_db

echo "start $(date '+%Y-%m-%d %H:%M:%S')"
cd /home/vagrant/nfiesta/nfiesta_htc
make && sudo make install && sudo -u vagrant make installcheck && make clean

cd /home/vagrant/nfiesta/nfiesta_pg
sudo make install && sudo -u vagrant make installcheck

PGPASSWORD=$APP_DB_PASS psql -h localhost -d $APP_DB_NAME -U $APP_DB_USER -c "create extension if not exists postgis;"
PGPASSWORD=$APP_DB_PASS psql -h localhost -d $APP_DB_NAME -U $APP_DB_USER -c "create extension if not exists plpython3u;"
PGPASSWORD=$APP_DB_PASS psql -h localhost -d $APP_DB_NAME -U $APP_DB_USER -c "create extension if not exists htc;"

echo "drop extension nfiesta";
PGPASSWORD=$APP_DB_PASS psql -h localhost -p 5432 -d $APP_DB_NAME -U $APP_DB_USER -c "DROP EXTENSION IF EXISTS nfiesta CASCADE;";
echo "create extension";
PGPASSWORD=$APP_DB_PASS psql -h localhost -p 5432 -d $APP_DB_NAME -U $APP_DB_USER -c "CREATE SCHEMA nfiesta;";
PGPASSWORD=$APP_DB_PASS psql -h localhost -p 5432 -d $APP_DB_NAME -U $APP_DB_USER -c "CREATE EXTENSION nfiesta WITH SCHEMA nfiesta;";

# Dump dynamic data
#select --*, 
#	concat('--table 'objid::regclass' \') as alter_command
#from pg_depend
#where refobjid = 20813 /*OID of extension diabolo*/ and classid = 1259 /*tables and sequences*/
#order by objid::regclass::text;
#--table analytical.c_aux_phase_type /
#--table analytical.c_aux_phase_type_id_seq /
#--table analytical.c_estimate_type /
#--table analytical.c_estimate_type_id_seq /
<<MULTI
PGPASSWORD=local_nfiesta_passwd pg_dump -h localhost --username adm_nfiesta --data-only --format custom --compress 9 --verbose \
--table nfiesta.c_area_domain \
--table nfiesta.c_area_domain_id_seq \
--table nfiesta.c_area_domain_category \
--table nfiesta.c_area_domain_category_id_seq \
--table nfiesta.c_auxiliary_variable \
--table nfiesta.c_auxiliary_variable_id_seq \
--table nfiesta.c_auxiliary_variable_category \
--table nfiesta.c_auxiliary_variable_category_id_seq \
--table nfiesta.c_country \
--table nfiesta.c_country_id_seq \
--table nfiesta.c_estimation_cell \
--table nfiesta.c_estimation_cell_id_seq \
--table nfiesta.c_estimation_cell_collection \
--table nfiesta.c_estimation_cell_collection_id_seq \
--table nfiesta.cm_cell2param_area_mapping \
--table nfiesta.cm_cell2param_area_mapping_id_seq \
--table nfiesta.cm_cluster2panel_mapping \
--table nfiesta.cm_cluster2panel_mapping_id_seq \
--table nfiesta.cm_plot2cell_mapping \
--table nfiesta.cm_plot2cell_mapping_id_seq \
--table nfiesta.cm_plot2cluster_config_mapping \
--table nfiesta.cm_plot2cluster_config_mapping_id_seq \
--table nfiesta.cm_plot2param_area_mapping \
--table nfiesta.cm_plot2param_area_mapping_id_seq \
--table nfiesta.cm_refyearset2panel_mapping \
--table nfiesta.cm_refyearset2panel_mapping_id_seq \
--table nfiesta.c_param_area_type \
--table nfiesta.c_param_area_type_id_seq \
--table nfiesta.c_state_or_change \
--table nfiesta.c_state_or_change_id_seq \
--table nfiesta.c_sub_population \
--table nfiesta.c_sub_population_id_seq \
--table nfiesta.c_sub_population_category \
--table nfiesta.c_sub_population_category_id_seq \
--table nfiesta.c_target_variable \
--table nfiesta.c_target_variable_id_seq \
--table nfiesta.c_variable_type \
--table nfiesta.c_variable_type_id_seq \
--table nfiesta.f_a_cell \
--table nfiesta.f_a_cell_gid_seq \
--table nfiesta.f_a_param_area \
--table nfiesta.f_a_param_area_gid_seq \
--table nfiesta.f_p_plot \
--table nfiesta.f_p_plot_gid_seq \
--table nfiesta.t_aux_conf \
--table nfiesta.t_aux_conf_id_seq \
--table nfiesta.t_auxiliary_data \
--table nfiesta.t_auxiliary_data_id_seq \
--table nfiesta.t_aux_total \
--table nfiesta.t_aux_total_id_seq \
--table nfiesta.t_cluster \
--table nfiesta.t_cluster_id_seq \
--table nfiesta.t_cluster_configuration \
--table nfiesta.t_cluster_configuration_id_seq \
--table nfiesta.t_estimate_conf \
--table nfiesta.t_estimate_conf_id_seq \
--table nfiesta.t_g_beta \
--table nfiesta.t_g_beta_id_seq \
--table nfiesta.t_inventory_campaign \
--table nfiesta.t_inventory_campaign_id_seq \
--table nfiesta.t_model \
--table nfiesta.t_model_id_seq \
--table nfiesta.t_model_variables \
--table nfiesta.t_model_variables_id_seq \
--table nfiesta.t_panel \
--table nfiesta.t_panel_id_seq \
--table nfiesta.t_panel2aux_conf \
--table nfiesta.t_panel2aux_conf_id_seq \
--table nfiesta.t_panel2total_1stph_est_data \
--table nfiesta.t_panel2total_1stph_est_data_id_seq \
--table nfiesta.t_panel2total_2ndph_est_data \
--table nfiesta.t_panel2total_2ndph_est_data_id_seq \
--table nfiesta.t_plot_measurement_dates \
--table nfiesta.t_plot_measurement_dates_id_seq \
--table nfiesta.t_reference_year_set \
--table nfiesta.t_reference_year_set_id_seq \
--table nfiesta.t_result \
--table nfiesta.t_result_id_seq \
--table nfiesta.t_strata_set \
--table nfiesta.t_strata_set_id_seq \
--table nfiesta.t_stratum \
--table nfiesta.t_stratum_id_seq \
--table nfiesta.t_target_data \
--table nfiesta.t_target_data_id_seq \
--table nfiesta.t_total_estimate_conf \
--table nfiesta.t_total_estimate_conf_id_seq \
--table nfiesta.t_total_estimate_data \
--table nfiesta.t_total_estimate_data_id_seq \
--table nfiesta.t_variable \
--table nfiesta.t_variable_id_seq \
--file nfiesta__2_0_4.backup -d nfiesta_db
MULTI

echo "loading data   -> analytical $(date '+%Y-%m-%d %H:%M:%S')";
#PGPASSWORD=local_nfiesta_passwd pg_restore -h localhost -p 5432 --single-transaction --exit-on-error -d nfiesta_db -U adm_nfiesta nfiesta__2_0_4.backup

echo "vacuum DB $(date '+%Y-%m-%d %H:%M:%S')"
PGPASSWORD=$APP_DB_PASS psql -h localhost -p 5432 -d $APP_DB_NAME -U $APP_DB_USER -c "VACUUM FULL ANALYZE;";

echo "finish $(date '+%Y-%m-%d %H:%M:%S')"

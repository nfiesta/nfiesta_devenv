#!/bin/sh -e

#-------------------------------------CACHING APT PACKAGES---------------------------------------------------
# on host sudo apt install squid-deb-proxy
# If host is running squid-deb-proxy on port 8000, populate /etc/apt/apt.conf.d/30proxy
# By default, squid-deb-proxy 403s unknown sources, so apt shouldn't proxy ppa.launchpad.net
#sudo apt update && apt install -y net-tools netcat
#sudo route -n | awk '/^0.0.0.0/ {print $2}' > /tmp/host_ip.txt
#sudo echo "HEAD /" | nc `cat /tmp/host_ip.txt` 8000 | grep squid-deb-proxy \
#        && (echo "Acquire::http::Proxy \"http://$(cat /tmp/host_ip.txt):8000\";" > /etc/apt/apt.conf.d/30proxy) \
#        && (echo "Acquire::http::Proxy::ppa.launchpad.net DIRECT;" >> /etc/apt/apt.conf.d/30proxy) \
#        || echo "No squid-deb-proxy detected on docker host"
#------------------------------------------------------------------------------------------------------------

# Edit the following to change the version of PostgreSQL that is installed
PG_VERSION=11
POSTGIS_VERSION=2.5

# Update package list and upgrade all packages
sudo apt update
# sudo apt -y full-upgrade

# install locale
sudo apt install -y locales
sudo sed -i 's/# en_US.UTF-8 UTF-8/en_US.UTF-8 UTF-8/' /etc/locale.gen
sudo sed -i 's/# cs_CZ.UTF-8 UTF-8/cs_CZ.UTF-8 UTF-8/' /etc/locale.gen
sudo locale-gen
# set default locale
#sudo update-locale LANG=en_US.UTF-8
#sudo update-locale LC_CTYPE=en_US.UTF-8

# install postgres & postgis
sudo apt install -y --no-install-recommends postgresql-$PG_VERSION postgresql-contrib-$PG_VERSION postgresql-client-$PG_VERSION postgresql-common libllvm6.0 sysstat
sudo apt install -y --no-install-recommends postgresql-$PG_VERSION-postgis-$POSTGIS_VERSION postgresql-$PG_VERSION-postgis-$POSTGIS_VERSION-scripts
sudo apt install -y --no-install-recommends postgresql-server-dev-$PG_VERSION # to build extension
sudo apt install -y --no-install-recommends postgresql-plpython3-$PG_VERSION python3-numpy
sudo apt install -y --no-install-recommends python3-psycopg2

# install python libraries
#sudo apt install -y --no-install-recommends python-sqlparse python-networkx python-pygraphviz

echo "Successfully created PostgreSQL dev virtual machine."

#!/bin/sh -e
#sudo apt install -y --no-install-recommends python3-venv
#sudo apt install -y --no-install-recommends build-essential libssl-dev libffi-dev libgmp3-dev virtualenv python3-pip libpq-dev python3-dev
sudo apt install -y python3-pip
#python3 -m venv pgadmin
#cd pgadmin
#source bin/activate
sudo -u vagrant wget -q https://ftp.postgresql.org/pub/pgadmin/pgadmin4/v3.5/pip/pgadmin4-3.5-py2.py3-none-any.whl
#pip3 install wheel
sudo -u vagrant pip3 install pgadmin4-3.5-py2.py3-none-any.whl

sudo -u vagrant sed -i "s/DEFAULT_SERVER = '127.0.0.1'/DEFAULT_SERVER = '0.0.0.0'/" .local/lib/python3.5/site-packages/pgadmin4/config.py

sudo -u vagrant echo "import os
DATA_DIR = os.path.realpath(os.path.expanduser(u'.local/pgadmin/'))
LOG_FILE = os.path.join(DATA_DIR, 'pgadmin4.log')
SQLITE_PATH = os.path.join(DATA_DIR, 'pgadmin4.db')
SESSION_DB_PATH = os.path.join(DATA_DIR, 'sessions')
STORAGE_DIR = os.path.join(DATA_DIR, 'storage')" > .local/lib/python3.5/site-packages/pgadmin4/config_local.py

#echo "python3 ~/.local/lib/python3.5/site-packages/pgadmin4/setup.py"
#echo "python3 ~/.local/lib/python3.5/site-packages/pgadmin4/pgAdmin4.py"

sed -i '1s/^/#!\/usr\/bin\/python3\n/' .local/lib/python3.5/site-packages/pgadmin4/pgAdmin4.py
chmod +x .local/lib/python3.5/site-packages/pgadmin4/pgAdmin4.py

echo "[Unit]
Description=Pgadmin4 Service
After=network.target
 
[Service]
User= vagrant
Group= vagrant
# Point to the virtual environment directory
WorkingDirectory=/home/vagrant
# Point to the bin folder of your virtual environment
Environment="PATH=/home/vagrant/.local/bin"
ExecStart="/home/vagrant/.local/lib/python3.5/site-packages/pgadmin4/pgAdmin4.py"
PrivateTmp=true
 
[Install]
WantedBy=multi-user.target" > /etc/systemd/system/pgadmin4.service

sudo -u vagrant cp -r /home/vagrant/DIABOLO/pgadmin/ .local/

service pgadmin4 start
systemctl enable pgadmin4

echo "Starting pgAdmin 4. Please navigate to http://0.0.0.0:5050 in your browser."
echo "pgAdmin 4 credentials: USER=pgadmin@nfiesta.eu PASS=pgadmin_nfiesta_passwd"

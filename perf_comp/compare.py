#%%
import re
import os
import json

import matplotlib.pyplot as plt
import pandas as pd

fd = {}
fd['files'] = []

directory = './txt/'
for filename in sorted(os.listdir(directory)):
    if filename.endswith(".txt"):
        td = {}
        td['file_name'] = filename
        td['tests'] = []
        for line in open(os.path.join(directory, filename), 'r'):
            if re.search('^test', line):
                line = re.sub(' +', ';', line)
                line = re.sub('\n', '', line)
                res = re.split(";", line)
                t = {}
                t['test_name'] = res[1]
                t['status'] = res[3]
                t['time'] = res[4]
                t['units'] = res[5]
                td['tests'].append(t)
        fd['files'].append(td) 

#print (json.dumps(fd, sort_keys=True, indent=4))
#%%
df_analytical = {}
for f in fd['files']:
    m = re.search(r'^(.*)__r([0-9])+\.txt$', f['file_name'])
    if m:
        platform = m.group(1)
        run = m.group(2)
    #print('p: %s    r:%s' % (platform, run))
    for t in f['tests']:
        #print('  %s %s' % (t['test_name'], t['time']))
        dd=df_analytical.setdefault(t['test_name'], {})
        if t['units'] == 'ms':
            dd.setdefault(platform,[]).append(float(t['time'])/1000)
        else:
            raise ValueError('Unknown unit %s.' % (t['units']))
        df_analytical[t['test_name']]=dd
# print (json.dumps(df_analytical, sort_keys=True, indent=4))
#%%

plt.close("all")
# for test_name, test_res in list(df_analytical.items())[0:2]:
for test_name, test_res in df_analytical.items():
    df = pd.DataFrame.from_dict(test_res)
    df.plot.box(xlabel="platform", ylabel="time [s]", title=test_name, rot=90, grid=True)
    plt.savefig('img/%s.svg' % (test_name), bbox_inches='tight')
    plt.close() 

#%%

small_dfs = []
#for test_name, test_res in list(df_analytical.items())[0:3]:
for test_name, test_res in df_analytical.items():
    small_dfs.append(pd.DataFrame.from_dict(test_res))

from functools import reduce
df_platforms = reduce(lambda x, y: x.add(y, fill_value=0), small_dfs)

df_platforms.plot.box(xlabel="platform", ylabel="time [s]", title='platforms' , rot=90, grid=True)
plt.savefig('img/%s.svg' % ('platforms'), bbox_inches='tight')

#%%

mean_dfs = []
#for test_name, test_res in list(df_analytical.items())[0:3]:
for test_name, test_res in df_analytical.items():
    s=pd.DataFrame.from_dict(test_res).mean(axis=1)
    s.name=test_name
    mean_dfs.append(s)

df_tests = pd.concat(mean_dfs, axis=1)
df_tests.plot.box(xlabel="test", ylabel="time [s]", title='tests' , rot=90, grid=True)#, logy=True)
plt.savefig('img/%s.svg' % ('tests'), bbox_inches='tight')

# %%
# comparison of PostgreSQL regression tests performance

## how-to run

```bash
sudo make install
make installcheck > my_configuration__r1.txt
make installcheck > my_configuration__r2.txt
make installcheck > my_configuration__r3.txt

mv my_configuration__r*.txt ~/nfiesta_devenv/perf_comp/txt
cd ~/nfiesta_devenv/perf_comp
python3 compare.py 
```

## results

## platforms

![platforms.svg](img/platforms.svg)

## tests

![tests.svg](img/tests.svg)

## other tests

[directory img](img)
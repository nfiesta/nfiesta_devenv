#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import sys
import datetime
import psycopg2
import psycopg2.extras
import json

try:
    connstr = 'dbname=db_source user=vagrant' # for local linux based authentication (without password)
    conn = psycopg2.connect(connstr)
except psycopg2.DatabaseError as e:
    print("nfiesta_parallel.single_query(), not possible to connect DB")
    print(e)
    sys.exit(1)

conn.set_session(isolation_level=None, readonly=None, deferrable=None, autocommit=True) # to not execute in transaction
cur = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)

sql_cmd = "select data_agg::text as jsondata from fn_export_agg(array[%s]);"
data = ([i+1 for i in range(100000)], )

try:
    print("%s\tetl_check.py processing command: %s start" % (datetime.datetime.now(), sql_cmd))
    cur.execute(sql_cmd, data)
    print("%s\tetl_check.py processing command: %s finished" % (datetime.datetime.now(), sql_cmd))
except psycopg2.Error as e:
    print("nfiesta_parallel.single_query(), SQL error:")
    print(e)
    sys.exit(1)

if cur.description != None:
    path_row_list = cur.fetchall()
else:
    path_row_list = None

print('size of data (text representation): %s MB' % (float(sys.getsizeof(path_row_list[0]['jsondata']))/(1024.0**2)))

obj = json.loads(path_row_list[0]['jsondata'])

print('size of data (python -- list & dict representation): %s MB' % (float(sys.getsizeof(obj))/(1024.0**2)))

'''
with open('data2.json', 'w') as f:
    json.dump(obj, f)

# Pretty Print JSON
json_formatted_str = json.dumps(obj, indent=4)
print(json_formatted_str)
'''

cur.close()
conn.close()

###########################################################

try:
    connstr = 'dbname=db_destination user=vagrant' # for local linux based authentication (without password)
    conn = psycopg2.connect(connstr)
except psycopg2.DatabaseError as e:
    print("nfiesta_parallel.single_query(), not possible to connect DB")
    print(e)
    sys.exit(1)


conn.set_session(isolation_level=None, readonly=None, deferrable=None, autocommit=True) # to not execute in transaction
cur = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)

sql_cmd = "select missing_agg from fn_import_agg(%s);"
data = (json.dumps(obj), )

try:
    print("%s\tetl_check.py processing command: %s start" % (datetime.datetime.now(), sql_cmd))
    cur.execute(sql_cmd, data)
    print("%s\tetl_check.py processing command: %s finished" % (datetime.datetime.now(), sql_cmd))
except psycopg2.Error as e:
    print("nfiesta_parallel.single_query(), SQL error:")
    print(e)
    sys.exit(1)

if cur.description != None:
    path_row_list = cur.fetchall()
else:
    path_row_list = None

for r in path_row_list:
    print(json.dumps(r['missing_agg'], indent=4))

cur.close()
conn.close()
